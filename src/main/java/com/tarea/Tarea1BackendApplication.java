package com.tarea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tarea1BackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tarea1BackendApplication.class, args);
	}
}
