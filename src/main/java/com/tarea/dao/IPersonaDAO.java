package com.tarea.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tarea.model.Persona;

public interface IPersonaDAO extends JpaRepository<Persona, Integer>{

}
