package com.tarea.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tarea.model.Producto;

public interface IProductoDAO extends JpaRepository<Producto, Integer>{

}
