package com.tarea.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tarea.model.Venta;

public interface IVentaDAO extends JpaRepository<Venta, Integer> {

}
