package com.tarea.service;

import com.tarea.model.Producto;
import com.tarea.service.ICRUD;

public interface IProductoService extends ICRUD<Producto> {

}
