package com.tarea.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tarea.dao.IVentaDAO;
import com.tarea.model.Venta;
import com.tarea.service.IVentaService;

@Service
public class VentaServiceImpl implements IVentaService {

	@Autowired
	private IVentaDAO dao;
	
	@Override
	public Venta listarId(int id) {
		return dao.findOne(id);
	}

	@Override
	public List<Venta> listar() {	
		return dao.findAll();
	}

	@Override
	public Venta registrar(Venta t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Venta modificar(Venta t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void eliminar(int id) {
		// TODO Auto-generated method stub
		
	}
	
}
